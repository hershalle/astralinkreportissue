//: Playground - noun: a place where people can play

import UIKit

//let user = User()
//let location = IssueLocation()
//let issueModel = IssueModel(id: "id", title: "title", description: "description", assignee: user, status: .closed, media: nil, location: location)
//
//
//let csvService = CSVService()
//let filePath = csvService.export(issueModels: [issueModel], toCSVFile: "filename")
//csvService.debug(filePath: filePath)
//

//
//let dateString = "Thu, 22 Oct 2015 07:45:17 +0000"
//let dateFormatter = DateFormatter()
//dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
//dateFormatter.locale = Locale.init(identifier: "en_GB")
//
//let dateObj = dateFormatter.date(from: dateString)
//
//dateFormatter.dateFormat = "MM-dd-yyyy"
//print("Dateobj: \(dateFormatter.string(from: dateObj!))")


extension Formatter {
    static let dateTimeFileName: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withDashSeparatorInDate, .withFullDate, .withColonSeparatorInTime, .withTime]
        return formatter
    }()
}

let dateString = Formatter.dateTimeFileName.string(from: Date()).replacingOccurrences(of: ":", with: "-")   // "2018-01-23T03:06:46.232Z"
//if let date = Formatter.iso8601.date(from: dateString)  {
//    print(date)   // "2018-01-23 03:06:46 +0000\n"
//}
//print(Date())
