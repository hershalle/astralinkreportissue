//
//  CSVService.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 02/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit
//import SwiftCSVExport

public struct CSVService {
    public init() {}
    
    private func csvRepresentation(fileName: String, header: [String], values: [[String: Any]]) -> CSV {
        let csv = CSV()
        csv.fields = header as NSArray
        csv.rows = values as NSArray
        csv.delimiter = DividerType.comma.rawValue
        csv.name = fileName
        return csv
    }

    private func export(csv: CSV) -> String {
        let csvOutput = CSVExport.export(csv)
        guard csvOutput.result.isSuccess, let filePath = csvOutput.filePath else {
            print("failed")
            return "failed"
        }

        print(filePath)
        return filePath
    }

    public func debug(filePath: String) {
        print(CSVExport.readCSV(filePath))
        guard let jsonIssueModel = CSVExport.readCSV(filePath)["rows"] as? [String: Any] else {
            print("error1")
            return
        }
        guard let data = jsonIssueModel.description.data(using: .utf8) else {
            print("error2")
            return
        }

    }
//    func importCSV(url: URL) {
//        guard let jsonIssueModel = CSVExport.readCSV(url.path)["rows"] as? [String: Any] else {
//            print("error")
//            return
//        }
//        guard let data = jsonIssueModel.description.data(using: .utf8) else {
//            print("error")
//            return
//        }
//
//        let issueModels: [IssueModel]? = try? JSONDecoder().decode([IssueModel].self, from: data)
//        print(issueModels)
//    }

    public func export(issueModels: [IssueModel], toCSVFile fileName: String) -> String {
        let headers = [(keyPath: \IssueModel.title, title: "Title"), (keyPath: \IssueModel.id, title: "id")]
        let values: [[String: Any]] = issueModels.map { (issueModel) -> [String: Any] in
            var row = [String: Any]()
            for header in headers {
                row[header.title] = issueModel[keyPath: header.keyPath]
            }
            return row
        }

        let headerTitles = headers.map {$0.title}
        let csvRepresentation = self.csvRepresentation(fileName: fileName, header: headerTitles, values: values)
        return export(csv: csvRepresentation)
    }
}
