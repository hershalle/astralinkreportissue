//
//  IssueModel.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 01/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

public struct IssueModel: Codable {
    public enum Status: String, Codable {
        case open
        case resolved
        case closed
        case reopend
    }

    var id: String
    var title: String
    var description: String
    var assignee: User
    var status: Status
    var media: MediaBundle?
    var location: IssueLocation

    public init(id: String,
    title: String,
    description: String,
    assignee: User,
    status: Status,
    media: MediaBundle?,
    location: IssueLocation) {
        self.id = id
        self.title = title
        self.description = description
        self.assignee = assignee
        self.status = status
        self.media = media
        self.location = location
    }
}

public struct User: Codable {
    public init() {}
}

public struct IssueLocation: Codable {
    public init() {}
}

public struct MediaBundle: Codable {

}

extension IssueModel: Comparable {
    public static func == (lhs: IssueModel, rhs: IssueModel) -> Bool {
        return lhs.id == rhs.id
    }

    public static func < (lhs: IssueModel, rhs: IssueModel) -> Bool {
        return lhs.id < rhs.id
    }
}
