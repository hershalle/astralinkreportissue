//
//  ModelViewController.swift
//  
//
//  Created by Shai Balassiano on 01/04/2018.
//

import UIKit
import GLKit

class ModelViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Model View Controller"
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nvc = segue.destination as? UINavigationController,
            let singleIssueViewController = nvc.topViewController as? SingleIssueViewController {
            let genericIssue = IssueModel(reportedBy: User(name: "namely name"), location: IssueLocation(viewerPoint: GLKVector3(v: (1, 2, 3))))
            singleIssueViewController.issueModel = genericIssue
        }
    }
}
