//
//  IssuesViewController.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 29/03/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit
import GLKit

class IssuesListViewController: UIViewController {

    @IBOutlet private var headerView: UIView!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var exportBarButtonItem: UIBarButtonItem!
    @IBOutlet private var headerViewLeftSapcer: UIView!
    @IBOutlet private var archiveButton: UIBarButtonItem!
    @IBOutlet private var selectionButton: UIBarButtonItem!
    @IBOutlet private var noIssuesView: UIView!

    private var completeDataSource = [IssueModel]()
    private var filteredDataSource = [IssueModel]() {
        didSet {
            updateVisibilityOfNoIssuesView()
        }
    }
    private var selectedRows = Set<Int>()

    override func viewDidLoad() {
        super.viewDidLoad()

        func setupSearchController() {
            navigationItem.searchController = UISearchController(searchResultsController: nil)
            navigationItem.searchController?.searchResultsUpdater = self
            navigationItem.searchController?.obscuresBackgroundDuringPresentation = false
            navigationItem.searchController?.searchBar.placeholder = "Search for issues"
            navigationItem.searchController?.definesPresentationContext = true
        }

        func constructMockDataSource() -> [IssueModel] {
            var result = [IssueModel]()
            for i in 0...20 {
                let genericIssue = IssueModel(titleId: "\(i)", title: "title\(i)", description: "", reportedBy: User(name: "user\(i)"), status: IssueModel.Status.open, media: MediaBundle(mediaResources: (0...10).map{_ in return MediaResource(image: R.image.one()!)}), location: IssueLocation(viewerPoint: GLKVector3(v: (1, 2, 3)), description: "location\(i)"))
                result.append(genericIssue)
            }
            return result
        }

        completeDataSource = constructMockDataSource()
        filteredDataSource = completeDataSource
        setupSearchController()
        tableView.sectionHeaderHeight = 61
        title = "Issue"
        self.headerViewLeftSapcer.isHidden = !self.isEditing
        updateRightBarButtonItems()
        tableView.tableFooterView = UIView()
        noIssuesView.layer.masksToBounds = true
        noIssuesView.layer.cornerRadius = 4
        updateVisibilityOfNoIssuesView()
    }

    private func updateVisibilityOfNoIssuesView() {
        noIssuesView?.isHidden = filteredDataSource.count != 0
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nvc = segue.destination as? UINavigationController,
            let singleIssueViewController = nvc.topViewController as? SingleIssueViewController,
            let indexPathForSelectedRow = tableView.indexPathForSelectedRow {
            singleIssueViewController.issueModel = filteredDataSource[indexPathForSelectedRow.row]
            singleIssueViewController.didSubmit = { (issueModel) in
                self.updateRow(data: issueModel, at: indexPathForSelectedRow)
            }
            singleIssueViewController.didDismiss = {
                if let indexPathForSelectedRow = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: indexPathForSelectedRow, animated: true)
                }
            }
        }
    }

    private func removeRow(in indexPath: IndexPath) {
        let issueModel = filteredDataSource.remove(at: indexPath.row)
        let indexInCompleteDataSource = completeDataSource.index(of: issueModel)!
        completeDataSource.remove(at: indexInCompleteDataSource)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    private func updateRow(data newIssueModel: IssueModel, at indexPath: IndexPath) {
        let oldIssueModel = filteredDataSource[indexPath.row]
        let indexInCompleteDataSource = completeDataSource.index(of: oldIssueModel)!

        completeDataSource.replaceSubrange(indexInCompleteDataSource...indexInCompleteDataSource, with: [newIssueModel])
        filteredDataSource.replaceSubrange(indexPath.row...indexPath.row, with: [newIssueModel])
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }

    private func updateRightBarButtonItems() {
        let items: [UIBarButtonItem] = isEditing ? [selectionButton, archiveButton] : [selectionButton, exportBarButtonItem]
        navigationItem.setRightBarButtonItems(items, animated: true)
    }

    @IBAction private func didTap(selectionButton: UIBarButtonItem) {
        isEditing = !isEditing
        selectionButton.title = isEditing ? "Done" : "Select"
        let visibleCells = tableView.visibleCells as! [IssuesListBodyTableViewCell]
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {
            self.headerViewLeftSapcer.isHidden = !self.isEditing
            for cell in visibleCells {
                cell.showLeftButton = self.isEditing
                cell.leftButtonSelected = false
            }
            self.view.layoutIfNeeded()
        }

        updateRightBarButtonItems()
        selectedRows.removeAll()
    }

    @IBAction private func didTap(archiveButton: UIBarButtonItem) {
        let rows = Array(selectedRows).sorted()
        selectedRows.removeAll()


        for (index, row) in rows.enumerated() {
            removeRow(in: IndexPath(row: row - index, section: 0))
        }
    }

    @IBAction private func didTap(exportButton: UIBarButtonItem) {
        let alertTitle: String
        let alertMessgae: String?
        if let _ = CSVService().export(issueModels: self.completeDataSource, toCSVFile: "Issues" + Date().stringDateTimeForFileName()) {
            alertTitle = "Export successful"
            alertMessgae = "You can find the file inside the Files app on your iPad"
        } else {
            alertTitle = "Export failed"
            alertMessgae  = nil
        }

        let alert = UIAlertController(title: alertTitle, message: alertMessgae, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            //  Do something here upon cancellation.
        })

        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}


extension IssuesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredDataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.issuesBodyTableViewCell, for: indexPath)!
        cell.configure(issueModel: filteredDataSource[indexPath.row])
        cell.showLeftButton = isEditing
        cell.leftButtonSelected = selectedRows.contains(indexPath.row)
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
}

extension IssuesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isEditing {
            let cell = tableView.cellForRow(at: indexPath) as! IssuesListBodyTableViewCell
            if selectedRows.contains(indexPath.row) {
                selectedRows.remove(indexPath.row)
            } else {
                selectedRows.insert(indexPath.row)
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        } else {
            performSegue(withIdentifier: R.segue.issuesListViewController.singleIssue.identifier, sender: self)
        }
    }
}

extension IssuesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let text = searchBar.text ?? ""
        filterSearchResults(text: text)
    }
}

extension IssuesListViewController: UISearchResultsUpdating {
    private func filterSearchResults(text: String) {
        guard !text.isEmpty else {
            filteredDataSource = completeDataSource
            tableView.reloadData()
            return
        }

        filteredDataSource.removeAll()

        filteredDataSource = completeDataSource.filter { (issueModel) -> Bool in
            return issueModel.title.lowercased().contains(text.lowercased())
        }

        tableView.reloadData()
    }

    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text ?? ""
        filterSearchResults(text: text)
    }
}
