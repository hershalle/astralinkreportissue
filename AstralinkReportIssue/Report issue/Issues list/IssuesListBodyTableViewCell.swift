//
//  IssuesBodyTableViewCell.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 01/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

class IssuesListBodyTableViewCell: UITableViewCell {
    @IBOutlet private var leftButton: UIButton!
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var idLabel: UILabel!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var createdAtLabel: UILabel!
    @IBOutlet private var locationDescriptionLabel: UILabel!
    @IBOutlet private var statusLabel: UILabel!

    var didTapLeftButton: (() -> ())?
    var showLeftButton = false {
        didSet {
            leftButton.isHidden = !showLeftButton
        }
    }

    var leftButtonSelected = false {
        didSet {
            leftButton.isSelected = leftButtonSelected
        }
    }
    func configure(issueModel: IssueModel) {
        if let titleId = issueModel.titleId {
            idLabel.text = "#\(titleId)"
        } else {
            idLabel.text = "New"
        }
        titleLabel.text = issueModel.title
        createdAtLabel.text = issueModel.createdAt.stringWithMediumDateStyle()
        locationDescriptionLabel.text = issueModel.location.description
        statusLabel.text = issueModel.status.rawValue.capitalized
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        showLeftButton = false
        leftButton.isSelected = false
    }

    @IBAction private func didTap(leftButton: UIButton) {
        leftButton.isSelected = !leftButton.isSelected
        didTapLeftButton?()
    }

}
