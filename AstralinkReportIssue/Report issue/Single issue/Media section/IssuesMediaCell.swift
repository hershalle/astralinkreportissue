//
//  IssuesMediaViewController.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 29/03/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation
import Eureka

protocol IssuesMediaCellNavigator: class {
    func presentImageViewer(image: UIImage)
    func presentImagePicker()
}

final class IssuesMediaCell: Cell<[UIImage]>, CellType, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet private var collectionView: UICollectionView!

    // injected:
    weak var navigator: IssuesMediaCellNavigator?

    var dataSource = [UIImage]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    override func setup() {
        super.setup()

        collectionView.register(R.nib.issuesMediaImageCollectionViewCell(), forCellWithReuseIdentifier: R.reuseIdentifier.issuesMediaImageCollectionViewCell.identifier)

        height = { return 174 }
    }

    // ===================================
    // MARK: - UICollectionViewDataSource:
    // ===================================
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.issuesMediaImageCollectionViewCell, for: indexPath)!
        cell.configure(image: dataSource[indexPath.row])
        return cell
    }

    // =================================
    // MARK: - UICollectionViewDelegate:
    // =================================
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigator?.presentImageViewer(image: dataSource[indexPath.row])
    }

    @IBAction private func didTap(addPhotoButton: UIButton) {
        navigator?.presentImagePicker()// { images in
//            self.dataSource.insert(contentsOf: images, at: 0)
//            self.collectionView.reloadData()
        //}
    }
}

final class IssuesMediaRowCell: Row<IssuesMediaCell>, RowType {
    var navigator: IssuesMediaCellNavigator? {
        get {
            let issuesMediaCell = baseCell as! IssuesMediaCell
            return issuesMediaCell.navigator
        }
        set {
            let issuesMediaCell = baseCell as! IssuesMediaCell
            issuesMediaCell.navigator = newValue
        }
    }

    override var value: [UIImage]? {
        didSet {
            let issuesMediaCell = baseCell as! IssuesMediaCell
            if let value = value {
                issuesMediaCell.dataSource = value
            } else {
                issuesMediaCell.dataSource = []
            }
        }
    }

    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
        cellProvider = CellProvider<IssuesMediaCell>(nibName: "IssuesMediaCell")
    }

}
