//
//  IssuesMediaImageCollectionViewCell.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 29/03/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

class IssuesMediaImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet private var imageView: UIImageView!

    func configure(image: UIImage) {
        imageView.image = image
    }
}
