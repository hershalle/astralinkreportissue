//
//  ViewController.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 29/03/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit
import Eureka

class SingleIssueViewController: FormViewController {
    enum Rows: String {
        case title = "Title"
        case status = "Status"
        case locationDescription = "Location description"
        case reportedBy = "Reported by"
        case trade = "Trade"
        case createdAt = "created at"
        case IssueDescription = "Description"
        case attachedPhotos = "Attached photos"
    }

    private var imagerPickerController = ImagePickerController()
    private let imageViewerController = R.storyboard.imageViewer.instantiateInitialViewController()!

    // injected:
    var issueModel: IssueModel!
    var didCancel: (() -> ())?
    var didSubmit: ((IssueModel) -> ())?
    var didDismiss: (() -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let issueModelId = issueModel.titleId {
            title = "Issue #\(issueModelId)"
        } else {
            title = "Issue"
        }

        let _ = constructForm()
    }

    private func constructForm() -> Form {
        return form +++
            TextRow(Rows.title.rawValue) { row in
                row.placeholder = Rows.title.rawValue
                row.title = Rows.title.rawValue
                row.value = issueModel.title
                row.cellUpdate({ (textCell, textRow) in
                    self.issueModel.title = textRow.value ?? ""
                })
            }
            <<< PushRow<String>(Rows.status.rawValue) { row in
                row.title = Rows.status.rawValue
                row.options = IssueModel.Status.values.map{$0.rawValue}
                row.value = self.issueModel.status.rawValue
                row.selectorTitle = "Change Status"
                row.cellUpdate({ (pushSelectorCell, pushRow) in
                    self.issueModel.status = IssueModel.Status(rawValue: pushRow.value!)!
                })
                }.onPresent { from, to in
                    to.dismissOnSelection = false
                    to.dismissOnChange = false
            }
            <<< TextRow(Rows.locationDescription.rawValue) { row in
                row.placeholder = Rows.locationDescription.rawValue
                row.title = Rows.locationDescription.rawValue
                row.value = self.issueModel.location.description
                row.cellUpdate({ (textCell, textRow) in
                    self.issueModel.location.description = textRow.value ?? ""
                })
            }
            <<< TextRow(Rows.reportedBy.rawValue) { row in
                row.placeholder = Rows.reportedBy.rawValue
                row.title = Rows.reportedBy.rawValue
                row.value = issueModel.reportedBy.name
                row.disabled = true
            }
            <<< TextRow(Rows.trade.rawValue) { row in
                row.placeholder = Rows.trade.rawValue
                row.title = Rows.trade.rawValue
                row.cellUpdate({ (textCell, textRow) in
                    self.issueModel.trade = textRow.value ?? ""
                })
            }
            <<< DateTimeRow() { row in
                row.title = Rows.createdAt.rawValue
                row.value = issueModel.createdAt
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .medium
                dateFormatter.timeStyle = .short
                row.dateFormatter = dateFormatter
                row.disabled = true
            }
            +++ Section(Rows.IssueDescription.rawValue)
            <<< TextAreaRow() { row in
                row.placeholder = "Enter your Description here"
                row.value = self.issueModel.description
                row.cellUpdate({ (textAreaCell, TextAreaRow) in
                    self.issueModel.description = TextAreaRow.value ?? ""
                })
            }
            <<< IssuesMediaRowCell(Rows.attachedPhotos.rawValue) {
                $0.navigator = self
                $0.value = self.issueModel.media.mediaResources.map{$0.image}
        }
    }

    @IBAction private func didTap(cancelButton: UIBarButtonItem) {
        dismiss(animated: true) {
            self.didCancel?()
            self.didDismiss?()
        }
    }

    @IBAction private func didTap(submitButton: UIBarButtonItem) {
        dismiss(animated: true) {
            self.didSubmit?(self.issueModel)
            self.didDismiss?()
        }
    }
}

extension SingleIssueViewController: IssuesMediaCellNavigator {
    func presentImagePicker() {
        imagerPickerController = ImagePickerController()
        imagerPickerController.present(on: self)
        imagerPickerController.completion = { (images: [UIImage]) in
            let newMediaResources = images.map{MediaResource(image: $0)}
            let oldMediaResources = self.issueModel.media.mediaResources
            self.issueModel.media.mediaResources = newMediaResources + oldMediaResources
            if let issuesMediaRowCell = self.form.rowBy(tag: Rows.attachedPhotos.rawValue) as? IssuesMediaRowCell {
                issuesMediaRowCell.value = self.issueModel.media.mediaResources.map{$0.image}
            }
        }
    }

    func presentImageViewer(image: UIImage) {
        imageViewerController.image = image
        imageViewerController.willDismiss = { (deleteImage) in
            guard deleteImage, let index = self.issueModel.media.mediaResources.index(of: image) else {
                return
            }

            self.issueModel.media.mediaResources.remove(at: index)
            let cell = self.form.rowBy(tag: Rows.attachedPhotos.rawValue) as? IssuesMediaRowCell
            cell?.value = self.issueModel.media.mediaResources.map{$0.image}
            cell?.reload()
        }
        present(imageViewerController, animated: true, completion: nil)
    }
}
