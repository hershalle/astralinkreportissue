//
//  ImageViewerController.swift
//  FilterComposer
//
//  Created by Shai Balassiano on 17/11/2017.
//  Copyright © 2017 hershalle. All rights reserved.
//

import UIKit

class ImageViewerController: UIViewController {
    @IBOutlet private var menuViewShowingConstraint: NSLayoutConstraint!
    @IBOutlet private var menuView: UIView!
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var imageView: UIImageView!
    
    private let imageSelectorController = ImagePickerController()
    // injected
    weak var image: UIImage! {
        didSet {
            imageView?.image = image
        }
    }
    var willDismiss: ((_ deleteImage: Bool) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        scrollView.maximumZoomScale = 2
        scrollView.minimumZoomScale = 1
        
        imageView.image = image
    }
    
    @IBAction private func didTap(dismissButton: UIButton) {
        let changedImage = image != imageView.image ? imageView.image : nil
        willDismiss?(false)
        dismiss(animated: true, completion: nil)
    }

    @IBAction private func didTap(deleteButton: UIButton) {
        let alert = UIAlertController(title: "Delete Image", message: "Are you sure you want to delete this image?", preferredStyle: .alert)

        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { (alert: UIAlertAction!) -> Void in
            self.willDismiss?(true)
            self.dismiss(animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            //  Do something here upon cancellation.
        })

        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func didTap(changeImageButton: UIButton) {
        imageSelectorController.present(on: self)
        imageSelectorController.imageLimit = 1
        imageSelectorController.didSetImage = { (images: [UIImage]) in
            if let firstImage = images.first {
                self.imageView.image = firstImage
                self.scrollView.zoomScale = 1
            }
        }
    }
}

extension ImageViewerController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let constant = menuView.frame.height - min(max(scrollView.contentOffset.y, 0), menuView.frame.height)
        menuViewShowingConstraint.constant = constant
        view.layoutIfNeeded()

        self.menuView.alpha = constant / menuView.frame.height
    }

    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        guard scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < menuView.frame.height else {
            return
        }

        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.4) {
            if scrollView.contentOffset.y < self.menuView.frame.height / 2 {
                self.menuViewShowingConstraint.constant =  self.menuView.frame.height
                scrollView.zoomScale = 1
                self.menuView.alpha = 1
            } else {
                self.menuViewShowingConstraint.constant = 0
                self.menuView.alpha = 0
            }
        }

        self.view.layoutIfNeeded()
    }
}
