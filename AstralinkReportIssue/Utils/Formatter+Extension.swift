//
//  Formatter.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 02/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

extension Formatter {
    static let timeFileName: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withColonSeparatorInTime, .withFullTime]
        return formatter
    }()

    static let dateFileName: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate]
        return formatter
    }()

    static let dateTimeFileName: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withDashSeparatorInDate, .withFullDate, .withColonSeparatorInTime, .withTime]
        return formatter
    }()
}


