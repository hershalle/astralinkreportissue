//
//  CSVService.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 02/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit
import SwiftCSVExport

struct CSVService {

    private func csvRepresentation(fileName: String, header: [String], values: [[String: Any]]) -> CSV {
        let csv = CSV()
        csv.fields = header as NSArray
        csv.rows = values as NSArray
        csv.delimiter = DividerType.comma.rawValue
        csv.name = fileName
        return csv
    }

    private func export(csv: CSV) -> String? {
        let csvOutput = CSVExport.export(csv)
        guard csvOutput.result.isSuccess, let filePath = csvOutput.filePath else {
            print("failed")
            return nil
        }

        print(filePath)
        return filePath
    }

    enum IssueModelHeaders: String {
        case id = "ID"
        case title = "Title"
        case dscription = "Description"
        case locationDescription = "Location"
        case status = "Status"
        case reportedBy = "Reported By"
        case createdAt = "Created At"
        case trade = "Trade"
        case photos = "#Photos"
        case lastUpdate = "Last Update"

        static var values: [IssueModelHeaders] {
            return [.id, .title, .dscription, .locationDescription, .status, .reportedBy, .createdAt, .trade, .photos, .lastUpdate]
        }

        static var stringValues: [String] {
            return values.map {$0.rawValue}
        }
    }

    func export(issueModels: [IssueModel], toCSVFile fileName: String) -> String? {
        let headers = IssueModelHeaders.stringValues
        let values = issueModels.map { (issueModel) -> [String: Any] in
            let id = issueModel.titleId ?? "New"
            let title = issueModel.title
            let dscription = issueModel.description
            let locationDescription = issueModel.location.description
            let status = issueModel.status.rawValue
            let createdAt = issueModel.createdAt.string(dateStyle: .medium, timeStyle: .short)
            let reportedBy = issueModel.reportedBy.name
            let photos = issueModel.media.mediaResources.count
            let lastUpdate = issueModel.updatedAt.string(dateStyle: .medium, timeStyle: .short)
            let trade = issueModel.trade

            let row: [String: Any] = [IssueModelHeaders.id.rawValue: id,
                                      IssueModelHeaders.title.rawValue: title,
                                      IssueModelHeaders.dscription.rawValue: dscription,
                                      IssueModelHeaders.locationDescription.rawValue: locationDescription,
                                      IssueModelHeaders.status.rawValue: status,
                                      IssueModelHeaders.createdAt.rawValue: createdAt,
                                      IssueModelHeaders.reportedBy.rawValue: reportedBy,
                                      IssueModelHeaders.trade.rawValue: trade,
                                      IssueModelHeaders.photos.rawValue: photos,
                                      IssueModelHeaders.lastUpdate.rawValue: lastUpdate]
            return row
        }

        let csvRepresentation = self.csvRepresentation(fileName: fileName, header: headers, values: values)
        return export(csv: csvRepresentation)
    }
}
