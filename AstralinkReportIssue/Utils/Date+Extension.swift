//
//  Date+Extension.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 02/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation

extension Date {
    func string(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = dateStyle
        dateFormatter.timeStyle = timeStyle
        return dateFormatter.string(from: self)
    }
    func stringWithMediumDateStyle() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: self)
    }

    func stringDateTimeForFileName() -> String {
        return Formatter.dateFileName.string(from: self)
    }
}
