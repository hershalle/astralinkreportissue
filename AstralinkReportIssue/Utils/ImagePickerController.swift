//
//  ImageSelector.swift
//  Closet
//
//  Created by Shai Balassiano on 16/10/2017.
//  Copyright © 2017 Shai Balassiano. All rights reserved.
//

import UIKit
import Gallery

class ImagePickerController: GalleryControllerDelegate {
    private let gallery = GalleryController()
    
    var didSetImage: ((_ images: [UIImage]) -> ())?
    var completion: ((_ images: [UIImage]) -> ())?
    var imageLimit: Int = 0 {
        didSet {
            Config.Camera.imageLimit = imageLimit
        }
    }
    
    init() {
        gallery.delegate = self
        setupConfiguration()
    }
    
    private func setupConfiguration() {
        Config.tabsToShow = [.cameraTab, .imageTab]
        Config.initialTab = .imageTab
        Config.Camera.imageLimit = imageLimit
    }
    
    func present(on viewController: UIViewController) {
        viewController.present(gallery, animated: true, completion: nil)
    }
    
    
    // ==================================
    // MARK: - GalleryControllerDelegate:
    // ==================================
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        Image.resolve(images: images) { (selectedImages: [UIImage?]) in
            let explicitSelectedImages: [UIImage] = selectedImages.filter({ (image: UIImage?) -> Bool in
                return image != nil
            }).map({$0!})
            
            self.didSetImage?(explicitSelectedImages)
            self.gallery.dismiss(animated: true) {
                self.completion?(explicitSelectedImages)
            }
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        // not supported
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        // not supported
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        gallery.dismiss(animated: true, completion: nil)
    }
}

