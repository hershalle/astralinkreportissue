//
//  UIView+Inspecatable.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 08/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var setRunTimeBackgroundColor: UIColor? {
        get {
            return backgroundColor
        }
        set {
            backgroundColor = newValue
        }
    }
}
