//
//  IssueModel.swift
//  AstralinkReportIssue
//
//  Created by Shai Balassiano on 01/04/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit
import GLKit

struct IssueModel: Codable {
    enum Status: String, Codable {
        case open = "Open"
        case resolved = "Resolved"
        case closed = "Closed"
        case reopend = "Repoend"

        static var values: [Status] {
            return [.open, .resolved, .closed, .reopend]
        }
    }

    var uuid: String
    var titleId: String?
    var title: String
    var description: String
    var reportedBy: User
    var status: Status
    var media: MediaBundle
    var location: IssueLocation
    var trade: String
    var createdAt: Date
    var updatedAt: Date

    init(uuid: String? = nil, titleId: String? = nil, title: String = "", description: String = "", reportedBy: User, status: Status = .open, media: MediaBundle = MediaBundle(), location: IssueLocation, trade: String = "") {
        self.uuid = uuid ?? UUID().uuidString
        self.titleId = titleId
        self.title = title
        self.description = description
        self.reportedBy = reportedBy
        self.status = status
        self.media = media
        self.location = location
        self.createdAt = Date()
        self.updatedAt = Date()
        self.trade = trade
    }

    init(viewerPoint: GLKVector3, user: User) {
        self.init(reportedBy: user, location: IssueLocation(viewerPoint: viewerPoint))
    }

    func updateFromServer(titleId: String, uuid: String) -> IssueModel {
        var newSelf = self
        newSelf.uuid = uuid
        newSelf.titleId = titleId
        return newSelf
    }
}

struct User: Codable {
    var name: String
}

struct IssueLocation: Codable {
    var viewerPoint: GLKVector3
    var description: String

    init(viewerPoint: GLKVector3, description: String = "") {
        self.viewerPoint = viewerPoint
        self.description = description
    }
}

struct MediaResource: Codable {
    var image: UIImage

    init(image: UIImage) {
        self.image = image
    }

    enum CodingKeys: String, CodingKey {
        case imageData
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let unarchivedImage: UIImage?
        if let imageData = try container.decodeIfPresent(Data.self, forKey: .imageData) {
            unarchivedImage = NSKeyedUnarchiver.unarchiveObject(with: imageData) as? UIImage
        } else {
            unarchivedImage = nil
        }

        if let unarchivedImage = unarchivedImage {
            image = unarchivedImage
        } else {
            throw NSError(domain: "Failed to construct UIImage", code: -1, userInfo: nil)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        let imageData = NSKeyedArchiver.archivedData(withRootObject: image)
        try container.encode(imageData, forKey: .imageData)
    }
}

extension MediaResource: Equatable {
    static func == (lhs: MediaResource, rhs: MediaResource) -> Bool {
        return lhs.image == rhs.image
    }
}

extension Array where Element == MediaResource {
    func index(of innerElement: UIImage) -> Int? {
        for (index, mediaResource) in self.enumerated() {
            if mediaResource.image == innerElement {
                return index
            }
        }
        return nil
    }
}

struct MediaBundle: Codable {
    var mediaResources = [MediaResource]()
}

extension IssueModel: Comparable {
    static func == (lhs: IssueModel, rhs: IssueModel) -> Bool {
        return lhs.uuid == rhs.uuid
    }

    static func < (lhs: IssueModel, rhs: IssueModel) -> Bool {
        return lhs.uuid < rhs.uuid
    }
}
